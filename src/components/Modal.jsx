import React from "react";

const Modal = () => {
  return (
    <React.Fragment>
      <div className="popup-icon">
        <button id="modBtn" className="modal-btn">
          <img src="img/contact-icon.png" alt="" />
        </button>
      </div>

      <div id="modal" className="modal">
        <div className="modal-content modal-animated-in">
          <div className="modal-header">
            <h3 className="header-title">
              Say hello to <em>Highway</em>
            </h3>
            <div className="close-btn">
              <img src="img/close_contact.png" alt="" />
            </div>
          </div>

          <div className="modal-body">
            <div className="col-md-6 col-md-offset-3">
              <form id="contact" action="" method="post">
                <div className="row">
                  <div className="col-md-12">
                    <input
                      name="name"
                      type="text"
                      className="form-control"
                      id="name"
                      placeholder="Your name..."
                      required=""
                    />
                  </div>
                  <div className="col-md-12">
                    <input
                      name="email"
                      type="email"
                      className="form-control"
                      id="email"
                      placeholder="Your email..."
                      required=""
                    />
                  </div>
                  <div className="col-md-12">
                    <textarea
                      name="message"
                      rows="6"
                      className="form-control"
                      id="message"
                      placeholder="Your message..."
                      required=""
                    ></textarea>
                  </div>
                  <div className="col-md-12">
                    <button type="submit" id="form-submit" className="btn">
                      Send Message Now
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Modal;
