import React from "react";

const Footer = () => {
  return (
    <footer>
      <div className="container-fluid">
        <div className="col-md-12">
          <p>Copyright &copy; 2018 XYZ Designs | Designed by Anas</p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
