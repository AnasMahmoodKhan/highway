import React from "react";

const Nav = () => {
  return (
    <React.Fragment>
      <nav>
        <div className="logo">
          <a href="index.html">
            High<em>way</em>
          </a>
        </div>
        <div className="menu-icon">
          <span></span>
        </div>
      </nav>
      <section className="overlay-menu">
        <div className="container">
          <div className="row">
            <div className="main-menu">
              <ul>
                <li>
                  <a href="index.html">Home - Full-width</a>
                </li>
                <li>
                  <a href="masonry.html">Home - Masonry</a>
                </li>
                <li>
                  <a href="grid.html">Home - Small-width</a>
                </li>
                <li>
                  <a href="/about">About Us</a>
                </li>
                <li>
                  <a href="/blog">Blog Entries</a>
                </li>
                <li>
                  <a href="/singlepost">Single Post</a>
                </li>
              </ul>
              
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

export default Nav;
