import React from "react";
import Services from "../pageComponents/Services";
const services = [
  {
    img: "img/service_1.png",
    h4: "CSS to React TEMPLATE",
    p:
      "Highway is a good CSS template that is available for free.Please tell your friends about templatemo site. Thank you.",
  },
  {
    img: "img/service_2.png",
    h4: "Aenean pellentesque",
    p:
      "Donec et nisi sed magna tincidunt fermentum. Pellentesque ege semper felis, sit amet scelerisque neque.",
  },
  {
    img: "img/service_3.png",
    h4: "Quisque et odio",
    p:
      "Donec et nisi sed magna tincidunt fermentum. Pellentesque ege semper felis, sit amet scelerisque neque.",
  },
  {
    img: "img/service_4.png",
    h4: "Nullam et justo",
    p:
      "Donec et nisi sed magna tincidunt fermentum. Pellentesque ege semper felis, sit amet scelerisque neque.",
  },
  {
    img: "img/service_5.png",
    h4: "Integer ac justo",
    p:
      "Donec et nisi sed magna tincidunt fermentum. Pellentesque ege semper felis, sit amet scelerisque neque.",
  },
  {
    img: "img/service_6.png",
    h4: "Integer ac justo",
    p:
      "Donec et nisi sed magna tincidunt fermentum. Pellentesque ege semper felis, sit amet scelerisque neque.",
  },
];

const About = () => {
  return (
    <React.Fragment>
      {" "}
      <div className="page-heading">
        <div className="container">
          <div className="heading-content">
            <h1>
              About <em>Us</em>
            </h1>
          </div>
        </div>
      </div>
      <div class="services">
        {" "}
        <div class="container">
          {services.map((service, i) => (
            <Services key={i} src={service.img} h4={service.h4} p={service.p} />
          ))}
        </div>
      </div>
      <div class="more-about-us">
        <div class="container">
          <div class="col-md-5 col-md-offset-7">
            <div class="content">
              <h2>Aenean vehicula tincidunt</h2>
              <span>Donec et nisi sed</span>
              <p>
                Vivamus vitae libero euismod, pretium magna a, vulputate metus.
                Curabitur et arcu felis. Praesent aliquet lectus in purus
                viverra varius.
                <br />
                <br />
                Suspendisse et rutrum nisl. Phasellus porttitor metus vel justo
                blandit, in finibus neque elementum. Nullam semper, turpis quis
                egestas consequat, dui eros tristique lectus, ac euismod ex quam
                id mauris.
              </p>
              <div class="simple-btn">
                <a href="#">Continue Reading</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="pricing-tables">
        <div class="container">
          <div class="col-md-4 col-sm-6">
            <div class="table-item">
              <h4>$250/mo</h4>
              <span>Starter Plan</span>
              <ul>
                <li>Xeon 8 Cores 3.2GHz</li>
                <li>RAM 32 GB</li>
                <li>10 TB RAID 1</li>
                <li>1,000 TB Transfer</li>
                <li>24-hour Support</li>
              </ul>
              <div class="simple-btn">
                <a href="#">Details</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="table-item premium-item">
              <h4>$420/mo</h4>
              <span>Standard Plan</span>
              <ul>
                <li>Xeon 16 Cores 3.2GHz</li>
                <li>RAM 64 GB</li>
                <li>20 TB RAID 1</li>
                <li>2,000 TB Transfer</li>
                <li>15-minute Quick Support</li>
              </ul>
              <div class="simple-btn">
                <a href="#">Details</a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-6">
            <div class="table-item">
              <h4>$750/mo</h4>
              <span>Premium Plan</span>
              <ul>
                <li>Xeon 32 Cores 3.2GHz</li>
                <li>RAM 128 GB</li>
                <li>10 TB SSD RAID 10</li>
                <li>6,000 TB Transfer</li>
                <li>1-minute Instant Support</li>
              </ul>
              <div class="simple-btn">
                <a href="#">Details</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default About;
