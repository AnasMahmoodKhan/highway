import React from "react";
import PortfolioImage from "../pageComponents/PortfolioImage";
const PortfolioImages = [
  {
    href: "img/big_portfolio_item_4.png",
    h1: "Biodiesel squid",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_4.png",
  },
  {
    href: "img/big_portfolio_item_2.png",
    h1: "raclette taxidermy",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_2.png",
  },
  {
    href: "img/big_portfolio_item_3.png",
    h1: "humblebrag brunch",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_3.png",
  },
  {
    href: "img/big_portfolio_item_1.png",
    h1: "Succulents chambray",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_1.png",
  },
  {
    href: "img/big_portfolio_item_5.png",
    h1: "freegan aesthetic",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_5.png",
  },
  {
    href: "img/big_portfolio_item_6.png",
    h1: "taiyaki vegan",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_6.png",
  },
  {
    href: "img/big_portfolio_item_7.png",
    h1: "Thundercats santo",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_7.png",
  },
  {
    href: "img/big_portfolio_item_8.png",
    h1: "wayfarers yuccie",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_8.png",
  },
  {
    href: "img/big_portfolio_item_9.png",
    h1: "",
    p: "Awesome Subtittle Goes Here",
    src: "img/portfolio_item_9.png",
  },
];
const Portfolio = () => {
  return (
    <div className="full-screen-portfolio" id="portfolio">
      <div className="container-fluid">
        {PortfolioImages.map((image, i) => (
          <PortfolioImage
            key={i}
            href={image.href}
            h1={image.h1}
            p={image.p}
            src={image.src}
          />
        ))}
      </div>
    </div>
  );
};

export default Portfolio;
