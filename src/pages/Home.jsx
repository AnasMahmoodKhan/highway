import React from "react";
import VideoComponent from "../pageComponents/VideoComponent";
import Portfolio from "./Portfolio";

const Home = () => {
  return (
    <React.Fragment>
      <VideoComponent />

      <Portfolio />
    </React.Fragment>
  );
};

export default Home;
