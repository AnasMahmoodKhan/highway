import { BrowserRouter, Route, Switch } from "react-router-dom";
import Footer from "./components/Footer";
import Modal from "./components/Modal";
import Nav from "./components/Nav";
import About from "./pages/About";
import Blog from "./pages/Blog";
import Home from "./pages/Home";
import SinglePost from "./pages/SinglePost";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Switch>
        <Route path="/about" component={About} />
        <Route path="/blog" component={Blog} />
        <Route path="/singlepost" component={SinglePost} />

        <Route path="/" component={Home} />
      </Switch>
      <Footer />
      <Modal />
    </BrowserRouter>
  );
}

export default App;
