import React from "react";

const VideoComponent = () => {
  return (
    <div id="video-container">
      <div className="video-overlay"></div>
      <div className="video-content">
        <div className="inner">
          <h1>
            Welcome to <em>Highway</em>
          </h1>
          <p>CSS Template used by Anas</p>
          <p>Homepage with full-width image gallery</p>
          <div className="scroll-icon">
            <a className="scrollTo" datascrollto="portfolio" href="#">
              <img src="img/scroll-icon.png" alt="" />
            </a>
          </div>
        </div>
      </div>
      <video autoplay="" loop="" muted>
        <source src="highway-loop.mp4" type="video/mp4" />
      </video>
    </div>
  );
};

export default VideoComponent;
