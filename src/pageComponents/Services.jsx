import React from "react";

const Services = ({src,h4,p}) => {
  return (
    <div className="col-md-4 col-sm-6">
      <div className="service-item">
        <div className="icon">
          <img src={src} alt="" />
        </div>
        <div className="text">
          <h4>{h4}</h4>
          <p>
            {p}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Services;
