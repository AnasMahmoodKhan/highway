import React from 'react'

const PortfolioImage = ({href,h1,p,src}) => {
    return (
        <div class="col-md-4 col-sm-6">
          <div class="portfolio-item">
            <a href={href} data-lightbox="image-1">
              <div class="thumb">
                <div class="hover-effect">
                  <div class="hover-content">
                    <h1>
                     {h1}
                    </h1>
                    <p>{p}</p>
                  </div>
                </div>
                <div class="image">
                  <img src={src} />
                </div>
              </div>
            </a>
          </div>
        </div>
        
    )
}

export default PortfolioImage
